import { Component, OnInit } from '@angular/core';
import { Autor } from 'src/app/models/Autor';
import { Editora } from 'src/app/models/Editora';
import { Livro } from 'src/app/models/Livro';
import { Funcionario } from 'src/app/models/Funcionario';
import { AutoresService } from 'src/app/services/autores/autores.service';
import { LivrosService } from 'src/app/services/livros/livros.service';
import { EditorasService } from 'src/app/services/editoras/editoras.service';
import { FuncionariosService } from 'src/app/services/funcionarios/funcionarios.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  autores: Autor[]
  livros: Livro[]
  editoras: Editora[]
  funcionarios: Funcionario[]

  constructor(
    private livrosService: LivrosService,
    private autoresService: AutoresService,
    private editorasService: EditorasService,
    private funcionariosService: FuncionariosService
  ) { 
    this.autores = []
    this.livros = [] 
    this.editoras = []
    this.funcionarios = []
  }

  ngOnInit(): void {

    this.autoresService.buscarAutores()
    .subscribe({
      next: (autores) => {
        this.autores = autores.results

        console.log(this.autores)
      },
      error: (erro) => {
        console.error(erro)
      }
    })

    this.livrosService.buscarLivros()
      .subscribe({
        next: (livros) => {
          this.livros = livros.results

          console.log(this.livros)
        },
        error: (erro) => {
          console.error(erro)
        }
      })

      this.editorasService.buscarEditoras()
      .subscribe({
        next: (editoras) => {
          this.editoras = editoras.results

          console.log(this.editoras)
        },
        error: (erro) => {
          console.error(erro)
        }
      })

      this.funcionariosService.buscarFuncionario()
      .subscribe({
        next: (funcionarios) => {
          this.funcionarios = funcionarios.results

          console.log(this.funcionarios)
        },
        error: (erro) => {
          console.error(erro)
        }
      })

    }
}
