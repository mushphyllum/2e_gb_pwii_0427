import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadAutorRoutingModule } from './cad-autor-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadAutorRoutingModule,
    FormsModule
  ]
})
export class CadAutorModule { }
