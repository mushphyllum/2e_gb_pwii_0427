import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadUsuarioComponent } from './cad-usuario.component';

const routes: Routes = [
  {
    path: "",
    component: CadUsuarioComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadUsuarioRoutingModule { }
