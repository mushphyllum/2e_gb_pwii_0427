import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-cad-usuario',
  templateUrl: './cad-usuario.component.html',
  styleUrls: ['./cad-usuario.component.scss']
})
export class CadUsuarioComponent implements OnInit {
  user: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) {
    this.user = new Usuario()
   }

  ngOnInit(): void {
  }

  cadastrarUsuario(): void {
    console.log(this.user)
    
    this.usuariosService.cadastrarUsuario(this.user)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }

}
