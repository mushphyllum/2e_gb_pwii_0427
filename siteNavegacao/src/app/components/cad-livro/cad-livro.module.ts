import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadLivroRoutingModule } from './cad-livro-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadLivroRoutingModule,
    FormsModule
  ]
})
export class CadLivroModule { }
