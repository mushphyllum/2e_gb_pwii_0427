import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/Usuario';
import { UsuariosService } from 'src/app/services/usuarios/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: Usuario

  constructor(
    private usuariosService: UsuariosService
  ) {
    this.user = new Usuario()
  }

  ngOnInit(): void {
    
  }

  logar(): void {
    console.log(this.user)
    
    this.usuariosService.logar(this.user)
    .subscribe({
      next: (dados) => {
        console.log(dados)
      },
      error: (erro) => {
        console.error(erro)
      }
    })
  }
}
