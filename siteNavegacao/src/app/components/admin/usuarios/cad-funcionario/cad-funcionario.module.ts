import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CadFuncionarioRoutingModule } from './cad-funcionario-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadFuncionarioRoutingModule,
    FormsModule
  ]
})
export class CadFuncionarioModule { }
