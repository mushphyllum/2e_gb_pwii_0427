import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadFuncionarioComponent } from './cad-funcionario.component';

const routes: Routes = [
  {
    path: "",
    component: CadFuncionarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadFuncionarioRoutingModule { }
