import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadEditoraRoutingModule } from './cad-editora-routing.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CadEditoraRoutingModule,
    FormsModule
  ]    
})
export class CadEditoraModule { }
