import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CadEditoraComponent } from './cad-editora.component';

const routes: Routes = [
  {
    path: "",
    component: CadEditoraComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CadEditoraRoutingModule { }
