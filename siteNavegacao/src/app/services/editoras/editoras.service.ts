import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Editora } from 'src/app/models/Editora';

@Injectable({
  providedIn: 'root'
})
export class EditorasService {
  private readonly URL_S = "https://3000-ygoroliveir-2egbapi0810-d6a6k3zyz8m.ws-us77.gitpod.io/"
  private readonly URL_Y = "https://3000-ygoroliveir-2egbapi0810-2fg2l12i56u.ws-us78.gitpod.io/"
  private readonly URL = this.URL_Y

  constructor(
    private http: HttpClient
  ) { }

  buscarEditoras(): Observable<any> {
    return this.http.get<any>(`${this.URL}editoras`)
  }


  cadastrarEditora(editora: Editora): Observable <any> {
    return this.http.post<any>(`${this.URL}editora`, editora)
  }
}

