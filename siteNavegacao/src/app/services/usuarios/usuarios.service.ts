import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from 'src/app/models/Usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {
  private readonly URL_S = "https://3000-ygoroliveir-2egbapi0810-3crydh1cnfr.ws-us78.gitpod.io/"
  private readonly URL_Y = "https://3000-ygoroliveir-2egbapi0810-0p39aurt7n5.ws-us78.gitpod.io/"
  private readonly URL = this.URL_S

  constructor(
    private http: HttpClient
  ) { }

  cadastrarUsuario(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario`, usuario)
  }

  logar(usuario: Usuario): Observable <any> {
    return this.http.post<any>(`${this.URL}usuario/login`, usuario)
  }
}