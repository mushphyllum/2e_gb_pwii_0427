import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Livro } from 'src/app/models/Livro';

@Injectable({
  providedIn: 'root'
})
export class LivrosService {
  private readonly URL_S = "https://3000-ygoroliveir-2egbapi0810-d6a6k3zyz8m.ws-us77.gitpod.io/"
  private readonly URL_Y = "https://3000-ygoroliveir-2egbapi0810-d6a6k3zyz8m.ws-us77.gitpod.io/"
  private readonly URL = this.URL_S

  constructor(
    private http: HttpClient
  ) { }

  buscarLivros(): Observable<any> {
    return this.http.get<any>(`${this.URL}livros`)
  }

  cadastrarLivro(livro: Livro): Observable <any> {
    return this.http.post<any>(`${this.URL}livro`, livro)
  }
}
