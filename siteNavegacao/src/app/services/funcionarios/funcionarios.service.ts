import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Funcionario } from 'src/app/models/Funcionario';

@Injectable({
  providedIn: 'root'
})
export class FuncionariosService {
  private readonly URL_S = "https://3000-ygoroliveir-2egbapi0810-d6a6k3zyz8m.ws-us77.gitpod.io/"
  private readonly URL_Y = "https://3000-ygoroliveir-2egbapi0810-2fg2l12i56u.ws-us79.gitpod.io/"
  private readonly URL = this.URL_Y

  constructor(
    private http: HttpClient
  ) { }

  buscarFuncionario(): Observable<any> {
    return this.http.get<any>(`${this.URL}funcionarios`)
  }

  cadastrarFuncionario(funcionario: Funcionario): Observable <any> {
    return this.http.post<any>(`${this.URL}funcionario`, funcionario)
  }
}

