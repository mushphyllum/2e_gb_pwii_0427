import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Autor } from 'src/app/models/Autor';

@Injectable({
  providedIn: 'root'
})
export class AutoresService {
  private readonly URL_S = "https://3000-ygoroliveir-2egbapi0810-3crydh1cnfr.ws-us78.gitpod.io/"
  private readonly URL_Y = "https://3000-ygoroliveir-2egbapi0810-d6a6k3zyz8m.ws-us77.gitpod.io/"
  private readonly URL = this.URL_S

  constructor(
    private http: HttpClient
  ) { }

  buscarAutores(): Observable<any> {
    return this.http.get<any>(`${this.URL}autores`)
  }

  cadastrarAutor(autor: Autor): Observable <any> {
    return this.http.post<any>(`${this.URL}autor`, autor)
  }
}
