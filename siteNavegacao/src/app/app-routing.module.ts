import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    pathMatch: "full",
    redirectTo: "home"
  },
  {
    path: "home", 
    loadChildren: () => import("./components/home/home.module").
    then(m => m.HomeModule)
  },
  {
    path: "cad-autor", 
    loadChildren: () => import("./components/cad-autor/cad-autor.module").
    then(m => m.CadAutorModule)
  },
  {
    path: "cad-editora", 
    loadChildren: () => import("./components/cad-editora/cad-editora.module").
    then(m => m.CadEditoraModule)
  },
  {
    path: "cad-livro", 
    loadChildren: () => import("./components/cad-livro/cad-livro.module").
    then(m => m.CadLivroModule)
  },
  {
    path: "cad-funcionario", 
    loadChildren: () => import("./components/admin/usuarios/cad-funcionario/cad-funcionario.module").
    then(m => m.CadFuncionarioModule)
  },
  {
    path: "cad-usuario", 
    loadChildren: () => import("./components/cad-usuario/cad-usuario.module").
    then(m => m.CadUsuarioModule)
  },
  {
    path: "adm-login", 
    loadChildren: () => import("./components/admin/login/login.module").
    then(m => m.LoginModule)
  },
  {
    path: "**",
    loadChildren: () => import("./components/page-not-found/page-not-found.module").
    then(m => m.PageNotFoundModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
