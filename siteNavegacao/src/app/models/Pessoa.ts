export class Pessoa {
    id: number = 0
    nome: string = ""
    telefone: string = ""
    rua: string = ""
    numero: string = ""
    bairro: string = ""
    cep: string = ""
    cidade: string = ""
    uf: string = ""
    tipo: string = ""
}
