import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CadAutorComponent } from './components/cad-autor/cad-autor.component';
import { CadEditoraComponent } from './components/cad-editora/cad-editora.component';
import { CadLivroComponent } from './components/cad-livro/cad-livro.component';
import { MenuComponent } from './components/commons/menu/menu.component';
import { FooterComponent } from './components/commons/footer/footer.component';
import { LivrosService } from './services/livros/livros.service';
import { AutoresService } from './services/autores/autores.service';
import { UsuariosComponent } from './components/admin/usuarios/usuarios.component';
import { CadFuncionarioComponent } from './components/admin/usuarios/cad-funcionario/cad-funcionario.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CadAutorComponent,
    CadEditoraComponent,
    CadLivroComponent,
    MenuComponent,
    FooterComponent,
    UsuariosComponent,
    CadFuncionarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    LivrosService,
    AutoresService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
